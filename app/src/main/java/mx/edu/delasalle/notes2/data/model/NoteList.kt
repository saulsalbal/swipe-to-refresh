package mx.edu.delasalle.notes2.data.model

data class NoteList(val data:List<Note> = listOf())

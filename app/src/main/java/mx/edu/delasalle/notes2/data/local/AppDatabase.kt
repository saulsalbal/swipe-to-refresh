package mx.edu.delasalle.notes2.data.local

import android.content.Context
import androidx.room.Database
import mx.edu.delasalle.notes2.data.model.NoteEntity
import java.security.AccessControlContext
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [NoteEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteDao


    companion object{
        private var INSTANCE: AppDatabase? = null

        fun getDataBase(context: Context) : AppDatabase{

            INSTANCE = INSTANCE?: Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "noteapp"
            ).build()

            return INSTANCE!!
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }


}
package mx.edu.delasalle.notes2.ui.notesdetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import mx.edu.delasalle.notes2.R
import mx.edu.delasalle.notes2.data.remote.ApiClient
import mx.edu.delasalle.notes2.data.remote.NoteDataSource
import mx.edu.delasalle.notes2.databinding.FragmentNoteEditBinding
import mx.edu.delasalle.notes2.presentation.NoteViewModel
import mx.edu.delasalle.notes2.presentation.NoteViewModelFactory
import mx.edu.delasalle.notes2.repository.NoteRepositoryImp

class NoteEditFragment : Fragment() {
    private lateinit var binding:FragmentNoteEditBinding

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(NoteRepositoryImp(NoteDataSource(ApiClient.service)))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = FragmentNoteEditBinding.bind(view)

        //binding.
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_note_edit2, container, false)
    }


}
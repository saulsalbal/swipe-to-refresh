package mx.edu.delasalle.notes2.repository

import mx.edu.delasalle.notes2.data.model.Note
import mx.edu.delasalle.notes2.data.model.NoteList

interface NoteRepository {

    suspend fun getNotes(): NoteList
    suspend fun saveNote(note: Note?) : Note?
}
package mx.edu.delasalle.notes2.data.model

data class Note(
    @DocumentId
    //val id:Int = 0,
    val id:Int = 0,
    val title:String = "",
    val content:String = "",
    val image:String = ""
)

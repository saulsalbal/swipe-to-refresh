package mx.edu.delasalle.notes2.data.remote

import com.google.gson.GsonBuilder
import mx.edu.delasalle.notes2.app.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    //Convertir el json
    val service by lazy{
        Retrofit.Builder()
            .baseUrl(Constants.BaseUrl)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(ApiService::class.java)

    }
}
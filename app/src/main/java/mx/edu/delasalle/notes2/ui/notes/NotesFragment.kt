package mx.edu.delasalle.notes2.ui.notes

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import mx.edu.delasalle.notes2.R
import mx.edu.delasalle.notes2.core.Resource
import mx.edu.delasalle.notes2.data.local.AppDatabase
import mx.edu.delasalle.notes2.data.local.LocalDataSource
import mx.edu.delasalle.notes2.data.model.Note
import mx.edu.delasalle.notes2.data.remote.ApiClient
import mx.edu.delasalle.notes2.data.remote.NoteDataSource
import mx.edu.delasalle.notes2.databinding.FragmentNotesBinding
import mx.edu.delasalle.notes2.presentation.NoteViewModel
import mx.edu.delasalle.notes2.presentation.NoteViewModelFactory
import mx.edu.delasalle.notes2.repository.NoteRepositoryImp
import mx.edu.delasalle.notes2.ui.notes.adapters.NoteAdapter

class NotesFragment : Fragment(R.layout.fragment_notes) {
    private lateinit var binding:FragmentNotesBinding
    private lateinit var adapter:NoteAdapter

    private val viewModel by viewModels<NoteViewModel> {
        NoteViewModelFactory(NoteRepositoryImp(
            LocalDataSource(AppDatabase.getDataBase(this.requireContext()).noteDao()),
            NoteDataSource(ApiClient.service)))
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentNotesBinding.bind(view)

        binding.recyclerNotes.layoutManager = GridLayoutManager(requireContext(), 2)

        binding.btnAddNote.setOnClickListener{

            val action = NotesFragmentDirections.actionNotesFragmentToNoteDetailFragment()
            findNavController().navigate(action)
        }
        viewModel.fetchNotes().observe(viewLifecycleOwner, Observer { result ->
            when(result){
                is Resource.Loading -> {
                    binding.progressbar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    binding.progressbar.visibility = View.GONE

                    adapter = NoteAdapter(result.data.data){ note ->
                        onNoteClick(note)
                    }

                    binding.recyclerNotes.adapter = adapter

                    Log.d("LiveData","${result.data.toString()}")
                }
                is Resource.Failure -> {
                    binding.progressbar.visibility = View.GONE
                    Log.d("LiveData","${result.exception.toString()}")
                }
            }
        })
    }

    private fun onNoteClick(note: Note){
        val action = NotesFragmentDirections.actionNotesFragmentToNoteDetailFragment(
            note.title,
            note.content,
            note.image,
            note.id
        )

        findNavController().navigate(action)
    }
}



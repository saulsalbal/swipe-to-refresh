package mx.edu.delasalle.notes2.data.remote

import mx.edu.delasalle.notes2.data.model.Note
import mx.edu.delasalle.notes2.data.model.NoteList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    //https://testapi.io/api/saulsalbal/resource/notess
    @GET("notess")
    suspend fun getNotes(): NoteList

    @POST("note")
    suspend fun saveNote(@Body note: Note?): Note?



}
package mx.edu.delasalle.notes2.data.local

import mx.edu.delasalle.notes2.data.model.*

class LocalDataSource( private val noteDao: NoteDao) {

    suspend fun getNotes(): NoteList = noteDao.getNotes().toNoteList()

    suspend fun saveNote(note: NoteEntity?) : Unit = noteDao.saveNote(note)
}
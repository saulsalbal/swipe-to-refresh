package mx.edu.delasalle.notes2.data.remote

import mx.edu.delasalle.notes2.data.model.Note
import mx.edu.delasalle.notes2.data.model.NoteList

class NoteDataSource (private val apiService: ApiService) {

    suspend fun getNotes(): NoteList = apiService.getNotes()

    suspend fun saveNote(note: Note?) : Note = apiService.saveNote(note)

    //Firebase
    /*
    suspend fun getNotes(): NoteList =
        firebase.notesRef().get().await().toNoteList()

    suspend fun saveNote(note:Note?) : Note?{
        note.let{
            firebase.noteRef().set(note!!).await()
        }
        return note
    }*/

}

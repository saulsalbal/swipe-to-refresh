package mx.edu.delasalle.notes2.data.extensions

import mx.edu.delasalle.notes2.data.model.Note
import mx.edu.delasalle.notes2.data.model.NoteList

fun QuerySnapshot.toNoteList(): NoteList{

    var lst = mutableListOf<Note>()

    for (document in this){
        var note = document.toObject(Note::class.java)
        lst.add(note)
    }

    return NoteList(lst)
}
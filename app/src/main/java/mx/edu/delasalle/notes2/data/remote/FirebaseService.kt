package mx.edu.delasalle.notes2.data.remote

class FirebaseService {

    fun collectionRef(collection:String) = FirebaseFirestore.getInstance().collection(collection)
    fun notesRef() = collectionRef("notes")
    fun noteRef() = notesRef().document()

}
package mx.edu.delasalle.notes2.repository

import mx.edu.delasalle.notes2.data.local.LocalDataSource
import mx.edu.delasalle.notes2.data.model.Note
import mx.edu.delasalle.notes2.data.model.NoteList
import mx.edu.delasalle.notes2.data.model.toNoteEntity
import mx.edu.delasalle.notes2.data.remote.NoteDataSource

class NoteRepositoryImp(
    private val localDataSource: LocalDataSource,
    private val dataSource: NoteDataSource) : NoteRepository {

    override suspend fun getNotes(): NoteList {
        dataSource.getNotes().data.forEach{ note ->
            localDataSource.saveNote(note.toNoteEntity())
        }

        return localDataSource.getNotes()
    }

    override suspend fun saveNote(note: Note?): Note? {
        return dataSource.saveNote(note)
    }
    }
